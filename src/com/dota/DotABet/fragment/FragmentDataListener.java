package com.dota.DotABet.fragment;

/**
 * Created with IntelliJ IDEA.
 * User: Imbalanxd
 * Date: 2014/03/29
 * Time: 7:35 PM
 * To change this template use File | Settings | File Templates.
 */
public interface FragmentDataListener
{
	public void onDataComplete(short _id);
}
